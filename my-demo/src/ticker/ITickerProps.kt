package ticker

interface ITickerProps {
    var startFrom: Int
    var name: String
}