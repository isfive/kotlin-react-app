import react.RBuilder
import react.RComponent
import react.RProps
import react.RState
import react.dom.*

class Result : RComponent<Result.Props, Result.State>() {

    override fun RBuilder.render() {
        div("result-item") {
            +"结果，结果"
            button(classes = "btn") {
                attrs{
                    title("复制")
                }

            }
        }
    }
    
    class Props() : RProps
    class State() : RState
}

fun RBuilder.result() = child(Result::class) {
}