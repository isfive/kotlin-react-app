package translate

import kotlinext.js.js
import kotlinx.html.ButtonType
import kotlinx.html.InputType
import kotlinx.html.js.onChangeFunction
import kotlinx.html.js.onClickFunction
import org.w3c.dom.HTMLInputElement
import react.*
import react.dom.*


interface TranslateState : RState {
    var text:String
    var items:List<String?>
}

class Translate(props: RProps): RComponent<RProps, TranslateState>(props) {

    override fun TranslateState.init(props:RProps) {
        text=""
        //items列表初始化
        items=listOf<String>()
        console.log(items)
    }
    override fun RBuilder.render() {
        div(classes = "warp"){
            h1 { +"TodoList" }
            input(type = InputType.text,classes = "search-input",name = "text"){
                key = "text"
                attrs {
                    placeholder="请输入内容"
                    value=state.text
                    onChangeFunction={
                        val target = it.target as HTMLInputElement
                        setState{
                            text=target.value
                        }
                    }
                }
            }
            button(classes = "search-btn"){
                +"确定"
                attrs {
                    onClickFunction={
                        setState{
                            items+=text
                            text=""
                        }
                    }
                }
            }
            div (classes = "resultWrap"){
                +"代办列表"
                attrs.jsStyle = js {
                    marginTop="30px"
                }
                ul {
                    for ((index,item) in state.items.withIndex()){
                        li {
                            key = index.toString()
                            +"${item}"
                            button(classes = "btn btn-danger",type = ButtonType.button) {
                                +"x"
                                attrs.jsStyle = js {
                                    marginLeft="10px"
                                }
                                attrs {
                                    onClickFunction = {
                                        setState {
                                            items = items.filterIndexed { i, _ -> i != index }
                                        }
                                    }
                                }
                            }
                            attrs.jsStyle = js {
                                marginTop="10px"
                            }
                        }
                    }
                }
            }
        }
    }
}





fun RBuilder.translate() = child(Translate::class) {

}