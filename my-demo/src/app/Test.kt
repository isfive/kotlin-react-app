import react.RBuilder
import react.RComponent
import react.RProps
import react.RState

class Test : RComponent<Test.Props, Test.State>() {

    override fun RBuilder.render() {

    }

    class Props() : RProps
    class State() : RState
}

fun RBuilder.test() = child(Test::class) {
}