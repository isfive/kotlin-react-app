package app

import kotlinx.html.ButtonType
import react.*
import react.dom.*
import logo.*
import ticker.*
import translate.*

class App : RComponent<RProps, RState>() {
    override fun RBuilder.render() {
//        div("App-header") {
//            logo()
//            h2 {
//                +"Welcome to React with Kotlin"
//            }
//        }
//        p("App-intro") {
//            +"To get started, edit "
//            code { +"app/App.kt" }
//            +" and save to reload."
//        }
//        div("App-ticker") {
//            ticker()
//        }
//        button (type = ButtonType.button,classes = "btn btn-primary"){
//            +"Primary"
//        }
        translate()
    }
}

fun RBuilder.app() = child(App::class) {}
